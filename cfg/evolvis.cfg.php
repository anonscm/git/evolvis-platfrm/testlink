<?php
/**
* TestLink Open Source Project - http://testlink.sourceforge.net/
*
* Connector to EvolvisForge – https://evolvis.org/projects/evolvis/
* Contributed by Thorsten Glaser <t.glaser@tarent.de>
*
* Based on GForge AS Plugin
* $Id: gforge.cfg.php,v 1.0.0.0 2008/08/21 17:12:00 john.wanke
* $Id: gforge.cfg.php,v 1.2 2008/11/04 19:58:22 franciscom Exp
*
* Contributed by john.wanke
*
* Constants used throughout TestLink are defined within this file
* they should be changed for your environment
*
*/

//Set the bug tracking system Interface to Evolvis (FusionForge) 4.8

/** The DB host to use when connecting to the FusionForge DB */
define('BUG_TRACK_DB_HOST', '127.0.0.1');

/** The name of the database that contains the FusionForge tables */
define('BUG_TRACK_DB_NAME', 'gforge');

/** The DB type being used by FusionForge
values: postgres
*/
define('BUG_TRACK_DB_TYPE', 'postgres');

/** The DB password to use for connecting to the FusionForge DB */
define('BUG_TRACK_DB_USER', 'gforge');
define('BUG_TRACK_DB_PASS', '');

/** The base URI of the Forge */
define('BUG_TRACK_BASE_URI', "https://evolvis-ff.tarent.de/");
?>
