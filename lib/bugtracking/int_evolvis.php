<?php
/**
 * TestLink Open Source Project - http://testlink.sourceforge.net/
 *
 * Connector to EvolvisForge – https://evolvis.org/projects/evolvis/
 * Contributed by Thorsten Glaser <t.glaser@tarent.de>
 *
 * Based on GForge AS Plugin
 * Filename $RCSfile: int_gforge.php,v
 *
 * @version $Revision: 1.3
 * @modified $Date: 2011-02-15 18:04:26 +0100 (Tue, 15 Feb 2011) $ $Author: schlundus
 *
 * @author John Wanke - 20080825 - initial revision.
 *
 * Constants used throughout TestLink are defined within this file
 * they should be changed for your environment
 *
**/
/** Interface name */
define('BUG_INTERFACE_CLASSNAME',"evolvisInterface");

class evolvisInterface extends bugtrackingInterface
{
	//members to store the bugtracking information
	var $dbHost = BUG_TRACK_DB_HOST;
	var $dbName = BUG_TRACK_DB_NAME;
	var $dbUser = BUG_TRACK_DB_USER;
	var $dbPass = BUG_TRACK_DB_PASS;
	var $dbType = BUG_TRACK_DB_TYPE;
	var $base_uri = BUG_TRACK_BASE_URI;

	/**
	 * Constructor of evolvisInterface
	 **/
	function evolvisInterface()
	{
		$this->bugtrackingInterface();
		$this->dbCharSet = "UTF-8";
		$this->data_valid = false;
	}

	/**
	 * simply returns the URL which should be displayed for entering bugs
	 *
	 * @return string returns a complete URL
	 **/
	function getEnterBugURL()
	{
		/*XXX if we had a group_id or similar, we could do much more */
		return $this->base_uri;
	}

	/**
	 * Return the URL to the bugtracking page for viewing
	 * the bug with the given id.
	 *
	 * @param int id the bug id
	 *
	 * @return string returns a complete URL to view the bug
	 **/
	function buildViewBugURL($id)
	{
		return $this->base_uri . "/tracker/t_follow.php/" . $id;
	}

	/**
	 * Returns the bug summary in a human readable format, cut down to 45 chars
	 *
	 * @return string returns the summary (in readable form) of the given bug
	 **/
	function getBugSummaryString($id)
	{
		if (!$this->checkBugID_existence($id))
			return null;

		$summary = $this->data_array['summary'];
		if (tlStringLen($summary) > 45) {
			$summary = tlSubStr($summary, 0, 42) . "...";
		}
		return $summary;
	}

	/**
	 * Returns the status in a readable form (HTML context) for the bug with the given id
	 *
	 * @param int id the bug id
	 *
	 * @return string returns the status (in a readable form) of the given bug
	 **/
	function getBugStatusString($id)
	{
		if (!$this->checkBugID_existence($id))
			return "<i>" . htmlspecialchars($id) . "?</i>";

		$status = $this->data_array['status_name'];

		$res = sprintf("%u", $id);

		/* strike through all bugs that have a resolved, verified, or closed status */
		if ($status == "Closed" || $status == "Deleted") {
			return "<del>" . $res . "</del>";
		}

		return $res;
	}

	/**
	 * checks is bug id is present on BTS
	 *
	 * @return bool
	 **/
	function checkBugID_existence($id)
	{
		/* needs to be numeric */
		if (!$this->checkBugID($id))
			return false;
		/* check cache */
		$id = (int)$id;
		if (isset($this->data_valid) &&
		    $this->data_valid === true &&
		    $this->data_id === $id)
			return true;

		/* need DB connection */
		if (!$this->isConnected())
			return false;

		/* get information about the bug, too */
		$query = "
			SELECT artifact_id, summary, status_name
			FROM artifact_vw
			WHERE artifact_id=$id
		    ;";
		$result = $this->dbConnection->exec_query($query);
		if (!$result)
			return false;
		if ($this->dbConnection->num_rows($result) != 1)
			/* not found */
			return false;
		$res = $this->dbConnection->fetch_array($result);
		if ($res && ($res['artifact_id'] == $id)) {
			$this->data_array = $res;
			$this->data_id = $id;
			$this->data_valid = true;
			return true;
		}

		/* eh? */
		return false;
	}
}
?>
